import numpy as np
import cv2
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from shading import find_bg_shade
from median import *
from mean import *
def shade_filter(image:np.ndarray,
                method="shading",
                extraction_size=20,
                extraction_color=(0,0),
                gain=2.0,
                noise_size=0.0,
                contrast_equalization=False):#画像処理を行う関数
    """
    ##### **image** :入力画像
    ##### **method**:補正方法
    "shading"でシェーディング補正
    "median"で中央値補正
    "mean"で平均値補正
    ##### **extraction_size**:抽出サイズ
    抽出したい欠陥のサイズを指定します。
    ##### **extraction_color** :抽出色
    (+10,0)で背景より10以上の明るさの欠陥を抽出
    (0,-10)で背景より10以下のくらさの欠陥を抽出
    (+10,-20)で背景を基準にして濃度が-20以下+10以上の範囲にある欠陥を抽出
    ##### **gain**:ゲイン
    0~10の値を指定します。デフォルトは2で，ゲインを大きくするとコントラストが上がります。
    ##### **noise_size**:除去するノイズ成分のサイズ
    ##### **contrast_equalization**:コントラスト均一化.
    TrueでON，FalseでOFF
    """
  
    if method=="shading":
        background_image=find_bg_shade(image,k=extraction_size)
        new_backgroud=np.full(image.shape,np.median(background_image).astype('uint8'))
    if method=="median":
        background_image=find_bg_median(image)
        new_backgroud=np.full(image.shape,np.median(background_image).astype('uint8'))
    if method=="mean":
        background_image=find_bg_mean(image)
        new_backgroud=np.full(image.shape,np.median(background_image).astype('uint8'))
    #抽出色
    extraction_filter=[1,1]#初期化
    if extraction_color[0]>0:
        extraction_filter[0]=image>background_image+extraction_color[0]#抽出すべき場所だけ1の行列
    else: extraction_filter[0]=False
    if extraction_color[1]<0:
        extraction_filter[1]=image<background_image+extraction_color[1]#抽出すべき場所だけ1の行列
    else: extraction_filter[1]=False
    extraction_filter=extraction_filter[0]+extraction_filter[1]#抽出すべき場所だけ1の行列どうしを足し合わせる

    #差分画像の生成
    image_diff =image.astype(int)-background_image.astype(int)
    image_diff=image_diff*extraction_filter##抽出色
    new_image=image_diff+new_backgroud
    #ゲイン設定
    max_pixcel=np.max(new_image,axis=(0,1,2))
    if max_pixcel>255:#maxが255を超えていたら修正
        new_image=255/max_pixcel*new_image
        max_pixcel=np.max(new_image,axis=(0,1,2))
    alpha=(255/max_pixcel-1)/(10-2)* (gain-2)+1
    new_image=alpha*new_image
    #ノイズ除去
    kernel=2*int(noise_size/2)+1
    new_image=cv2.GaussianBlur(new_image,(kernel,kernel),0)
    if contrast_equalization:#コントラスト均一化
        max=[[np.max(new_image,axis=(0,1))]]
        max=np.array(max)
        min=[[np.min(new_image,axis=(0,1))]]
        min=np.array(min)
        new_image=(new_image-min)*255/(max-min)#ヒストグラムの拡張
    return new_image




if __name__ == "__main__":
    img = cv2.imread("test.png")
    dst = shade_filter(img,extraction_color=(0,-10),gain=5.0,contrast_equalization=True)
    cv2.imwrite("./dst.png", dst)