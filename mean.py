import numpy as np
import cv2

def find_bg_mean(image: np.ndarray):
    
    average=[[np.average(image,axis=(0,1))]]#平均を算出
    average=np.array(average)#ndarrayに変換
    dst=image*0+average
    return dst
    
if __name__ == "__main__":
    img = cv2.imread("test.png")
    dst = find_bg_mean(img)
    cv2.imwrite("./bg_test.png", dst)