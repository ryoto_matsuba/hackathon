import numpy as np
import cv2
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

def find_bg_shade(image,k=10):
    
    #中央値を取る近傍の範囲
    d = int((k - 1) / 2)
    #入力画像の高さ, 幅
    h, w = image.shape[0], image.shape[1]

    dst = image.copy()
    for y in range(d, h - d):
        for x in range(d, w - d):
            dst[y][x] = np.median(image[y-d:y+d+1, x-d:x+d+1])#近傍の中央値を代入

    return dst

if __name__ == "__main__":
    img = cv2.imread("test.png")
    dst = find_bg_shade(img,k=20)
    cv2.imwrite("./bg_test.png", dst)
