# Hait-hackathon0
緊急ハッカソン
## ファイルの構成
* main.py:メイン(shade_filter)の処理
* median.py,mean.py,shading.py:補正関数の実装
* Untitled.ipynb関数の実行のための試行錯誤

補正オプションを引数にした関数shade＿filterを実行することで,返り値として補正画像を返します。
引数の説明はmain.pyで説明してあります。
