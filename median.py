import numpy as np
import cv2
def find_bg_median(image: np.ndarray, k=10):
    median=[[np.median(image,axis=(0,1))]]#平均を算出
    amedian=np.array(median)#ndarrayに変換
    dst=image*0+median
    return dst
if __name__ == "__main__":
    img = cv2.imread("test.png")
    dst = find_bg_median(img)
    cv2.imwrite("./bg_test.png", dst)
